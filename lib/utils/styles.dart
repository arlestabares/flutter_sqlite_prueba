import 'package:flutter/material.dart';

final ButtonStyle textButtonStyle = TextButton.styleFrom(
  primary: Colors.white,
  backgroundColor: Colors.grey,
  padding: const EdgeInsets.symmetric(horizontal: 16.0),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(16.0),
  ),
);