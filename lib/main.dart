import 'package:flutter/material.dart';
import 'package:flutter_sqlite/features/blocs/multi_bloc_provider.dart';
import 'package:flutter_sqlite/features/repositories/repository_provider.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/memory_repository.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/repository.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/sqlite_repository.dart';
import 'package:flutter_sqlite/routes/routes.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final repository = SqliteRepository();
  final mRepository = MemoryRepository();
  await mRepository.init();
  await repository.init();
  runApp(MyApp(repository: repository, mRepository: mRepository));
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required this.repository,
    required this.mRepository,
  }) : super(key: key);
  final Repository repository;
  final MemoryRepository mRepository;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProviderWidget(
      repository: repository,
      mRepository: mRepository,
      child: MultiBlocProviderWidget(
        child: MaterialApp(
          title: 'Material App',
          initialRoute: 'home_sqlite_page',
          routes: appRoutes,
        ),
      ),
    );
  }
}
