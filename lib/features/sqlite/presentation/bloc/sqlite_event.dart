part of 'sqlite_bloc.dart';

abstract class Event extends Equatable {
  const Event();

  @override
  List<Object> get props => [];
}

class SqliteInitialEvent extends Event {}

class SqliteSendEvent extends Event {}

class SqliteUserLoadEvent extends Event {
  final String user;

  const SqliteUserLoadEvent(this.user);
}

class SqlitePasswordLoadEvent extends Event {
  final String password;

  const SqlitePasswordLoadEvent(this.password);
}

class SqliteClientLoadEvent extends Event {}

class SqliteInsertOrderEvent extends Event {
  final Order order;

  const SqliteInsertOrderEvent(this.order);
}

class SqliteDeleteOrderEvent extends Event {
  final Order order;

  const SqliteDeleteOrderEvent(this.order);
}

class SqliteOrderLoadEvent extends Event {}

class SqliteVendorLoadEvent extends Event {}

class SqliteProductsCatalogLoadEvent extends Event {
  final Client client;

  const SqliteProductsCatalogLoadEvent(this.client);
}
