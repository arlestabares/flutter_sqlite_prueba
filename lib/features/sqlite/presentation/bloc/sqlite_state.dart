part of 'sqlite_bloc.dart';

abstract class SqliteState extends Equatable {
  const SqliteState(this.model);
  final Model model;

  @override
  List<Object> get props => [model];
}

class SqliteInitialState extends SqliteState {
  const SqliteInitialState(Model model) : super(model);
}

class SqliteSendedState extends SqliteState {
  const SqliteSendedState(Model model) : super(model);
}

class SqliteUserLoadSuccessState extends SqliteState {
  const SqliteUserLoadSuccessState(Model model) : super(model);
}

class SqlitePassworLoadedSuccessState extends SqliteState {
  const SqlitePassworLoadedSuccessState(Model model) : super(model);
}

class SqliteClientLoadSuccessState extends SqliteState {
  const SqliteClientLoadSuccessState(Model model) : super(model);
}

class SqliteInsertOrderSuccessState extends SqliteState {
  const SqliteInsertOrderSuccessState(Model model) : super(model);
}

class SqliteDeleteOrderSuccessState extends SqliteState {
  const SqliteDeleteOrderSuccessState(Model model) : super(model);
}

class SqliteOrderLoadSuccessState extends SqliteState {
  const SqliteOrderLoadSuccessState(Model model) : super(model);
}

class SqliteVendorLoadSuccessState extends SqliteState {
  const SqliteVendorLoadSuccessState(Model model) : super(model);
}

class SqliteProductsCatalogLoadSuccessState extends SqliteState {
  const SqliteProductsCatalogLoadSuccessState(Model model) : super(model);
}

class Model extends Equatable {
  final String? user;
  final String? clientName;
  final String? password;
  final Order? order;
  final Client? client;
  final List<Client>? clientList;
  final List<Order>? orderList;
  final List<Vendedor>? vendorList;
  final List<ProductosCatalog>? productsCatalog;
  const Model({
    this.user,
    this.clientName,
    this.password,
    this.order,
    this.orderList,
    this.client,
    this.clientList,
    this.vendorList,
    this.productsCatalog,
  });

  Model copyWith({
    String? user,
    String? password,
    String? clientName,
    Order? order,
    Client? client,
    List<Order>? orderList,
    List<Client>? clientList,
    List<Vendedor>? vendorList,
    List<ProductosCatalog>? productsCatalog,
  }) {
    return Model(
      user: user ?? this.user,
      clientName: clientName ?? this.clientName,
      password: password ?? this.password,
      order: order ?? this.order,
      orderList: orderList ?? this.orderList,
      client: client ?? this.client,
      clientList: clientList ?? this.clientList,
      vendorList: vendorList ?? this.vendorList,
      productsCatalog: productsCatalog ?? this.productsCatalog,
    );
  }

  @override
  List<Object?> get props => [
        user,
        password,
        clientList,
        vendorList,
        productsCatalog,
        order,
        clientName,
        client,
        orderList
      ];
}
