import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/client.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/order.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/productos_catalogo.dart';
// import 'package:flutter_sqlite/features/sqlite/data/models/user.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/vendedor.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/memory_repository.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/sqlite_repository.dart';

part 'sqlite_event.dart';
part 'sqlite_state.dart';

class SqliteBloc extends Bloc<Event, SqliteState> {
  final SqliteRepository sqliteRepository;
  final MemoryRepository mRepository;
  SqliteBloc(
      [SqliteRepository? sqliteRepository, MemoryRepository? mRepository])
      : sqliteRepository = sqliteRepository ??= SqliteRepository(),
        mRepository = mRepository ??= MemoryRepository(),
        super(initialState) {
    on<SqliteUserLoadEvent>(_onUploadSqliteUserEvent);
    on<SqlitePasswordLoadEvent>(_onUploadSqlitePasswordEvent);
    on<SqliteClientLoadEvent>(_onUploadSqliteClientEvent);
    on<SqliteVendorLoadEvent>(_onUploadSqliteVendedorEvent);
    on<SqliteProductsCatalogLoadEvent>(_onUploadSqliteProductscatalogEvent);
    on<SqliteSendEvent>(_onButtonLoginEvent);
    on<SqliteInsertOrderEvent>(_onInsertOrderEvent);
    on<SqliteOrderLoadEvent>(_onUploadOrderEvent);
    on<SqliteDeleteOrderEvent>(_onDeleteOrderEvent);
  }
  static SqliteState get initialState => const SqliteInitialState(Model());

  _onUploadSqliteUserEvent(
      SqliteUserLoadEvent event, Emitter<SqliteState> emit) async {
    String user = '';
    final userList = await sqliteRepository.getAllUser();

    for (var element in userList) {
      user = element.usuario!;
    }
    if (event.user == user) {
      emit(SqliteUserLoadSuccessState(state.model.copyWith(user: event.user)));
    }
  }

  _onUploadSqlitePasswordEvent(
      SqlitePasswordLoadEvent event, Emitter<SqliteState> emit) async {
    String password = '';
    final userList = await sqliteRepository.getAllUser();
    for (var element in userList) {
      password = element.passsword!;
    }
    if (event.password == password) {
      emit(SqlitePassworLoadedSuccessState(
          state.model.copyWith(password: event.password)));
    }
  }

  _onUploadSqliteClientEvent(
      SqliteClientLoadEvent event, Emitter<SqliteState> emit) {
    sqliteRepository.dbHelper.getDatabase;
    emit(SqliteClientLoadSuccessState(state.model));
  }

  _onUploadSqliteVendedorEvent(
      SqliteVendorLoadEvent event, Emitter<SqliteState> emit) {
    emit(SqliteVendorLoadSuccessState(state.model));
  }

  _onUploadSqliteProductscatalogEvent(
      SqliteProductsCatalogLoadEvent event, Emitter<SqliteState> emit) async {
    final productCatalog = await sqliteRepository.getAllProductsCatalog();

    emit(SqliteProductsCatalogLoadSuccessState(
      state.model.copyWith(
        client: event.client,
        clientName: event.client.nombre,
        productsCatalog: productCatalog,
      ),
    ));
  }

  _onButtonLoginEvent(SqliteSendEvent event, Emitter<SqliteState> emit) async {
    final vendorList = await sqliteRepository.getAllVendors();
    final clientList = await sqliteRepository.getAllClients();
    emit(SqliteSendedState(
        state.model.copyWith(vendorList: vendorList, clientList: clientList)));
  }

  _onInsertOrderEvent(
      SqliteInsertOrderEvent event, Emitter<SqliteState> emit) async {
    final id = await sqliteRepository.insertOrder(event.order);

    emit(SqliteInsertOrderSuccessState(
        state.model.copyWith(order: event.order)));
  }

  _onUploadOrderEvent(
      SqliteOrderLoadEvent event, Emitter<SqliteState> emit) async {
    final orderList = await sqliteRepository.getAllOrders();
    emit(SqliteOrderLoadSuccessState(
        state.model.copyWith(orderList: orderList)));
  }

  _onDeleteOrderEvent(
      SqliteDeleteOrderEvent event, Emitter<SqliteState> emit) async {
    await sqliteRepository.deleteOrder(event.order);
    emit(SqliteDeleteOrderSuccessState(state.model));
  }
}
