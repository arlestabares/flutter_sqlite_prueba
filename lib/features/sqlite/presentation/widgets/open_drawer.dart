import 'package:flutter/material.dart';
import 'package:flutter_sqlite/utils/const.dart';

AppBar openDrawer(String title) {
  return AppBar(
    title: Text(title),
    backgroundColor: bgColor,
    leading: Builder(
      builder: (context) => IconButton(
        icon: const Icon(Icons.menu,size: 41.0),
        onPressed: () => Scaffold.of(context).openDrawer(),
      ),
    ),
  );
}