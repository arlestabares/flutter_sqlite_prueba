import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/bloc/sqlite_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/details_view.dart';

class HomeSqlitePage extends StatelessWidget {
  const HomeSqlitePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                ...[
                  const UserBlocBuild(),
                  PasswordBlocBuild(),
                  ButtonBlocBuild(
                    formKey: formKey,
                  ),
                ].expand(
                  (widget) => [
                    widget,
                    const SizedBox(height: 24.0),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class UserBlocBuild extends StatelessWidget {
  const UserBlocBuild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController userController = TextEditingController();
    return BlocBuilder<SqliteBloc, SqliteState>(
      builder: (context, state) {
        return TextField(
          controller: userController,
          onChanged: (value) {
            context.read<SqliteBloc>().add(SqliteUserLoadEvent(value));
          },
          decoration: const InputDecoration(
            labelText: 'Usuario',
            hintText: 'Usuario',
          ),
        );
      },
    );
  }
}

// ignore: must_be_immutable
class PasswordBlocBuild extends StatelessWidget {
  PasswordBlocBuild({Key? key}) : super(key: key);
  TextEditingController passwordController = TextEditingController();
  String inputString = '';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SqliteBloc, SqliteState>(
      builder: (context, state) {
        return TextField(
          controller: passwordController,
          onChanged: (value) {
            // inputString = value;
            context.read<SqliteBloc>().add(SqlitePasswordLoadEvent(value));
          },
          onSubmitted: (_) => addPassword(context),
          decoration: const InputDecoration(
            labelText: 'Password',
            hintText: 'Password',
          ),
        );
      },
    );
  }

  void addPassword(context) {
    passwordController.text = '';
    // context.read<SqliteBloc>().add(SqlitePasswordLoadEvent(inputString));
  }
}

class ButtonBlocBuild extends StatelessWidget {
  const ButtonBlocBuild({Key? key, this.formKey}) : super(key: key);
  final GlobalKey<FormState>? formKey;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SqliteBloc, SqliteState>(
      builder: (context, state) {
        return ElevatedButton(
          child: const Text('Login'),
          onPressed: () async {
            // if (!(formKey?.currentState?.validate() ?? false)) {
            //   log('${formKey?.currentState?.validate()}');
            //   return;
            // }
            if (state.model.user == null || state.model.password == null) {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Login',textAlign: TextAlign.center,),
                  content: const Text('Ingrese Usuario y Contraseña'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'Cancel'),
                      child: const Text('Cancel'),
                    ),
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: const Text('OK'),
                    ),
                  ],
                ),
              );
              return;
            }

            context.read<SqliteBloc>().add(SqliteSendEvent());
            // Navigator.push(
            //   context,
            // MaterialPageRoute<void>(
            //   builder: (BuildContext context) => const DetailsView(),
            // ),
            // );
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => const DetailsView(),
                ),
                (route) => false);
          },
        );
      },
    );
  }
}
