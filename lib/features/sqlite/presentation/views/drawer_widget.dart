import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/bloc/sqlite_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/views.dart';
import 'package:flutter_sqlite/utils/const.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0);
    return SizedBox(child: BlocBuilder<SqliteBloc, SqliteState>(
      builder: (context, state) {
        return Drawer(
          backgroundColor: bgColor,
          child: ListView(
            children: <Widget>[
              const UserAccountsDrawerHeader(
                decoration: BoxDecoration(color: bgColor),
                currentAccountPicture: FlutterLogo(),
                accountName: Text('Arles Tabares', style: style),
                accountEmail: Text(
                  'arlestabares@gmail.com',
                  style: style,
                ),
              ),
              const Divider(
                height: 13.0,
                color: Colors.grey,
              ),
              const SizedBox(height: 8.0),
              const Text(
                'Sqlite',
                style: TextStyle(color: Colors.white, fontSize: 18.0),
                textAlign: TextAlign.center,
              ),
              _CreateDrawerItem(
                icon: Icons.production_quantity_limits,
                text: 'Pedidos realizados',
                onTap: () {
                  context.read<SqliteBloc>().add(SqliteOrderLoadEvent());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: ((context) => const OrdersLoadView()),
                      ),
                      (route) => false);
                },
              ),
            ],
          ),
        );
      },
    ));
  }
}

class _CreateDrawerItem extends StatelessWidget {
  const _CreateDrawerItem({
    Key? key,
    required this.icon,
    required this.text,
    required this.onTap,
  }) : super(key: key);
  final IconData icon;
  final String text;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Row(
        children: [
          Icon(icon, color: Colors.white),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              text,
              style: const TextStyle(color: Colors.white, fontSize: 18.0),
            ),
          ),
        ],
      ),
      onTap: onTap,
    );
  }
}
