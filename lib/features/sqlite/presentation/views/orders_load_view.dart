import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/details_view.dart';
import 'package:flutter_sqlite/utils/const.dart';

import '../bloc/sqlite_bloc.dart';

class OrdersLoadView extends StatelessWidget {
  const OrdersLoadView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: bgColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            context.read<SqliteBloc>().add(SqliteSendEvent());
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: ((context) => const DetailsView())),
                (route) => false);
          },
        ),
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 21.0),
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          children: const <Widget>[
            Expanded(
              child: OrdersBlocBuild(),
            ),
          ],
        ),
      ),
    );
  }
}

class OrdersBlocBuild extends StatelessWidget {
  const OrdersBlocBuild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style =
        TextStyle(fontSize: 18.0, color: bgColor, fontWeight: FontWeight.bold);
    return BlocBuilder<SqliteBloc, SqliteState>(
      buildWhen: (_, state) => state is SqliteOrderLoadSuccessState,
      builder: (context, state) {
        if (state is SqliteOrderLoadSuccessState) {
          return ListView.builder(
            itemCount: state.model.orderList?.length,
            itemBuilder: (context, index) {
              final order = state.model.orderList?[index];
              return Dismissible(
                key: ValueKey<int>(order!.id!),
                direction: DismissDirection.endToStart,
                background: Container(
                  color: Colors.red,
                  alignment: Alignment.centerRight,
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: const Icon(Icons.delete_forever),
                ),
                onDismissed: (DismissDirection direction) async {
                  context.read<SqliteBloc>().add(
                        SqliteDeleteOrderEvent(order),
                      );
                },
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(21.0),
                  margin: const EdgeInsets.only(bottom: 12.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(21.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Cliente: ${order.client}', style: style),
                      Text(
                        'Nombre Producto: ${order.productName}',
                        style: style,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text('Precio Producto: ${order.productPrice} Pesos',
                          style: style),
                      Text('Codigo Vendedor: ${order.productVendor}',
                          style: style),
                      Text('Fecha De la Orden: ${order.productOrderDate}',
                          style: style),
                    ],
                  ),
                ),
              );
            },
          );
        }
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
