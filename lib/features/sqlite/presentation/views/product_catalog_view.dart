import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/models.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/bloc/sqlite_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/details_view.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/widgets/build_appbar.dart';
import 'package:flutter_sqlite/utils/styles.dart';
import 'package:intl/intl.dart';

class ProductCatalogView extends StatelessWidget {
  const ProductCatalogView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final String cliente = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: buildAppBar(context, () {
        context.read<SqliteBloc>().add(SqliteSendEvent());
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: ((context) => const DetailsView())),
            (route) => false);
      }),
      body: Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: const <Widget>[
            Expanded(child: ProductCatalogBlocBuild()),
          ],
        ),
      ),
    );
  }
}

class ProductCatalogBlocBuild extends StatelessWidget {
  const ProductCatalogBlocBuild({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(now);
    // print(formatted); // something like 2021-06-19
    const style = TextStyle(
        color: Colors.black, fontSize: 21.0, fontWeight: FontWeight.bold);

    return BlocListener<SqliteBloc, SqliteState>(
      listener: (context, state) {
        if (state is SqliteInsertOrderSuccessState) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Orden guardada exitosamente'),
            ),
          );
        }
      },
      child: BlocBuilder<SqliteBloc, SqliteState>(
        buildWhen: (_, state) => state is SqliteProductsCatalogLoadSuccessState,
        builder: (context, state) {
          if (state is SqliteProductsCatalogLoadSuccessState) {
            return ListView.builder(
              shrinkWrap: true,
              itemCount: state.model.productsCatalog?.length,
              itemBuilder: (context, int index) {
                final productCatalog = state.model.productsCatalog?[index];
                return Container(
                  padding: const EdgeInsets.all(21.0),
                  margin: const EdgeInsets.only(bottom: 12.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 1.0),
                    borderRadius: BorderRadius.circular(21.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Center(
                        child: Text('Producto', style: style),
                      ),
                      const SizedBox(height: 16.0),
                      Text('Nombre: ${productCatalog?.nombre}', style: style),
                      Text('Precio: ${productCatalog?.precio}', style: style),
                      Text('Vendedor: ${productCatalog?.vendedor}',
                          style: style),
                      const SizedBox(
                        height: 18.0,
                      ),
                      Center(
                        child: TextButton(
                          onPressed: () async {
                            String? idOrder = productCatalog?.codigo;
                            int id = int.parse(idOrder!);
                            final order = Order(
                              id: id,
                              client: state.model.client?.nombre,
                              productName: productCatalog?.nombre,
                              productPrice: productCatalog?.precio,
                              productVendor: productCatalog?.vendedor,
                              productOrderDate: formatted,
                            );
                            if ((state.model.order?.id != id) &&
                                (state.model.order?.productName !=
                                    productCatalog?.nombre)) {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text(
                                    'Guardar Pedido',
                                    textAlign: TextAlign.center,
                                  ),
                                  content: const Text(
                                    'Esta seguro de Guardar este pedido?',
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Cancel'),
                                      child: const Text('Cancel'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        context
                                            .read<SqliteBloc>()
                                            .add(SqliteInsertOrderEvent(order));
                                        context
                                            .read<SqliteBloc>()
                                            .add(SqliteSendEvent());
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const DetailsView()),
                                            ),
                                            (route) => false);
                                      },
                                      child: const Text('OK'),
                                    ),
                                  ],
                                ),
                              );
                              return;
                            } else {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: const Text(
                                    'Guardar Pedido',
                                    textAlign: TextAlign.center,
                                  ),
                                  content: const Text(
                                    'Este pedido ya se encuentra guardado',
                                    style: TextStyle(color: Colors.red),
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Regresar'),
                                      child: const Text('Regresar'),
                                    ),
                                  ],
                                ),
                              );
                              return;
                            }
                          },
                          style: textButtonStyle,
                          child: const Text('Guadar Pedido', style: style),
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.blue,
              strokeWidth: 12.0,
            ),
          );
        },
      ),
    );
  }
}
