import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/bloc/sqlite_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/views.dart';

import '../widgets/widgets.dart';

class DetailsView extends StatelessWidget {
  const DetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: openDrawer('Sqlite Prueba'),
      drawer: const DrawerWidget(),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: const <Widget>[
                VendedorBlocBuilder(),
                Expanded(child: ClientBlocBuilder()),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class VendedorBlocBuilder extends StatelessWidget {
  const VendedorBlocBuilder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold);
    return BlocBuilder<SqliteBloc, SqliteState>(
      // buildWhen: ((_, state) => state is SqliteSendedState),
      builder: (context, state) {
        if (state is SqliteSendedState) {
          return ListView.builder(
            shrinkWrap: true,
            itemCount: state.model.vendorList?.length,
            itemBuilder: (context, int index) {
              final vendor = state.model.vendorList?[index];
              return Container(
                padding: const EdgeInsets.all(21.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 2.0),
                    borderRadius: BorderRadius.circular(21.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Text('Bodega: ${vendor?.bodega}', style: style),
                    // Text('Codigo: ${vendor?.codigo}', style: style),
                    const Center(
                      child: Text(
                        'Informacionde Usuario',
                        style: style,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(height: 16.0),
                    Text('Nombre: ${vendor?.nombre}', style: style),
                    Text('Fecha Labores: ${vendor?.fechaLabores}',
                        style: style),
                    // Text('Fecha Consecutivo: ${vendor?.fechaConsecutivo}',
                    //     style: style),
                    // Text('Consecutivo: ${vendor?.consecutivo}', style: style),
                    Text('Empresa: ${vendor?.empresa}', style: style),
                    // Text('Distrito: ${vendor?.distrito}', style: style),
                    // Text(
                    //   'Portafolio: ${vendor?.portafolio}',
                    //   style: style,
                    //   maxLines: 5,
                    // ),
                  ],
                ),
              );
            },
          );
        }
        return const Center(
            child: CircularProgressIndicator(color: Colors.red));
      },
    );
  }
}

class ClientBlocBuilder extends StatelessWidget {
  const ClientBlocBuilder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold);
    return BlocBuilder<SqliteBloc, SqliteState>(
      builder: (context, state) {
        if (state is SqliteSendedState) {
          return ListView.builder(
            shrinkWrap: true,
            itemCount: state.model.clientList?.length,
            itemBuilder: (context, int index) {
              final client = state.model.clientList?[index];
              return GestureDetector(
                onTap: () {
                  context.read<SqliteBloc>().add(
                        SqliteProductsCatalogLoadEvent(client!),
                      );
                  Navigator.pushNamed(context, 'product_catalog',
                      arguments: client.nombre);
                },
                child: Container(
                  padding: const EdgeInsets.all(21.0),
                  margin: const EdgeInsets.only(bottom: 12.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 1.0),
                    borderRadius: BorderRadius.circular(21.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Center(
                        child: Text('Cliente', style: style),
                      ),
                      const SizedBox(height: 16.0),
                      Text('Nombre: ${client?.nombre}', style: style),
                      Text('Ciudad: ${client?.ciudad}', style: style),
                      Text('Razon Social: ${client?.razonSocial}',
                          style: style),
                    ],
                  ),
                ),
              );
            },
          );
        }
        return const Center(
            child: CircularProgressIndicator(color: Colors.red));
      },
    );
  }
}
