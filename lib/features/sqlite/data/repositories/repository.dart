
import '../models/models.dart';

abstract class Repository {
  Future<int> insertOrder(Order order);
  Future<List<User>> getAllUser();
  Future<List<Order>> getAllOrders();
  Future<void> deleteOrder(Order order);
  Future<List<Client>> getAllClients();
  Future<List<Vendedor>> getAllVendors();
  Future<List<ProductosCatalog>> getAllProductsCatalog();

  Future init();
  void close();
}
