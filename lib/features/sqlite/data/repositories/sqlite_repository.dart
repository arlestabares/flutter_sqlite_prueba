import 'package:flutter_sqlite/features/sqlite/data/repositories/repository.dart';
import 'package:flutter_sqlite/features/sqlite/data/sqlite/database_helper.dart';

import '../models/models.dart';

class SqliteRepository extends Repository {
  final dbHelper = DatabaseHelper.instance;

  @override
  Future<List<User>> getAllUser() {
    return dbHelper.getAllUsers();
  }

  @override
  Future<List<Client>> getAllClients() {
    return dbHelper.getAllClients();
  }

  @override
  Future<List<Vendedor>> getAllVendors() {
    return dbHelper.getAllVendors();
  }

  @override
  Future<List<ProductosCatalog>> getAllProductsCatalog() {
    return dbHelper.getAllProductsCatalog();
  }

  @override
  Future<int> insertOrder(Order order) {
    return Future(() async {
      final id = await dbHelper.insertOrder(order);
      order.id = id;
      return id;
    });
  }

  @override
  Future<List<Order>> getAllOrders() {
    return dbHelper.getAllOrder();
  }

  @override
  Future<int> deleteOrder(Order order) async {
    return await dbHelper.deleteOrder(order);
  }

  @override
  Future init() async {
    await dbHelper.getDatabase;
    return Future.value();
  }

  @override
  void close() {
    dbHelper.close();
  }
}
