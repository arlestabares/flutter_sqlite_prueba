import 'dart:core';

import 'package:flutter_sqlite/features/sqlite/data/repositories/repository.dart';

import '../models/models.dart';

class MemoryRepository extends Repository {
  final List<Client> _clientList = <Client>[];
  final List<Order> _orderList = <Order>[];
  final List<User> _usersList = <User>[];
  final List<Vendedor> _vendedorList = <Vendedor>[];
  final List<ProductosCatalog> _productscatalog = <ProductosCatalog>[];

  @override
  Future<List<Client>> getAllClients() {
    return Future.value(_clientList);
  }

  @override
  Future<List<ProductosCatalog>> getAllProductsCatalog() {
    return Future.value(_productscatalog);
  }

  @override
  Future<List<User>> getAllUser() {
    return Future.value(_usersList);
  }

  @override
  Future<List<Vendedor>> getAllVendors() {
    return Future.value(_vendedorList);
  }

  @override
  Future<int> insertOrder(Order order) {
    return Future.value(0);
  }
    @override
  Future<List<Order>> getAllOrders() {
   return Future.value(_orderList);
  }
  
  @override
  Future<void> deleteOrder(Order order) {
   return Future.value(0);
  }

  @override
  Future init() {
    return Future.value();
  }

  @override
  void close() {}
  

}
