import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String? usuario;
  final String? passsword;
  final String? empresa;

  const User({
    this.usuario,
    this.passsword,
    this.empresa,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        usuario: json['usuario'] ?? '',
        passsword: json['password'] ?? '',
        empresa: json['empresa'] ?? '',
      );

  @override
  List<Object?> get props => [usuario, passsword, empresa];
}
