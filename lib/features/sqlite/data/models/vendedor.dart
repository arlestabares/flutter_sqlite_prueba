import 'package:equatable/equatable.dart';

class Vendedor extends Equatable {
  final String? bodega;
  final String? codigo;
  final String? nombre;
  final String? fechaLabores;
  final String? fechaConsecutivo;
  final int? consecutivo;
  final String? empresa;
  final String? distrito;
  final String? portafolio;
  final String? moneda;
  final int? tipo;

  const Vendedor({
    this.bodega,
    this.codigo,
    this.nombre,
    this.fechaLabores,
    this.fechaConsecutivo,
    this.consecutivo,
    this.empresa,
    this.distrito,
    this.portafolio,
    this.moneda,
    this.tipo,
  });

  factory Vendedor.fromJson(Map<String, dynamic> json) => Vendedor(
        bodega: json['bodega'] ?? '',
        codigo: json['codigo'] ?? '',
        nombre: json['nombre'] ?? '',
        fechaLabores: json['fechaLabores'] ?? '',
        fechaConsecutivo: json['fechaConsecutivo'] ?? '',
        consecutivo: json['consecutivo'] ?? 0,
        empresa: json['empresa'] ?? '',
        distrito: json['distrito'] ?? '',
        portafolio: json['portafolio'] ?? '',
        moneda: json['moneda'] ?? '',
        tipo: json['tipo'] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        'bodega': bodega,
        'codigo': codigo,
        'nombre': nombre,
        'fechaLabores': fechaLabores,
        'fechaConsecutivo': fechaConsecutivo,
        'consecutivo': consecutivo,
        'empresa': empresa,
        'distrito': distrito,
        'portafolio': portafolio,
        'moneda': moneda,
        'tipo': tipo,
      };

  @override
  List<Object?> get props => [
        bodega,
        codigo,
        nombre,
        fechaLabores,
        fechaConsecutivo,
        consecutivo,
        empresa,
        distrito,
        portafolio,
        moneda,
        tipo,
      ];
}
