import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Order extends Equatable {
  int? id;
  final String? client;
  final String? productName;
  final double? productPrice;
  final String? productVendor;
  final String? productOrderDate;

  Order({
    this.id,
    this.client,
    this.productName,
    this.productPrice,
    this.productVendor,
    this.productOrderDate,
  });

  factory Order.fromJson(Map<dynamic, dynamic> json) => Order(
        id: json['orderId'],
        client: json['client'] ?? '',
        productName: json['productName'] ?? '',
        productPrice: json['productPrice'] ?? 0.0,
        productVendor: json['productVendor'] ?? '',
        productOrderDate: json['productOrderDate'] ?? '',
      );

  Map<String, dynamic> toJson() => {
        'orderId': id,
        'client': client,
        'productName': productName,
        'productPrice': productPrice,
        'productVendor': productVendor,
        'productOrderDate': productOrderDate,
      };

  @override
  List<Object?> get props =>
      [client, productName, productPrice, productVendor, productOrderDate];
}
