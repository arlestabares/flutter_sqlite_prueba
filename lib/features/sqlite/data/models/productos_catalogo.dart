import 'package:equatable/equatable.dart';

class ProductosCatalog extends Equatable {
  final String? codigo;
  final String? nombre;
  final double? precio;
  // final String? unidadMedida;
  // final String? linea;
  // final String? marca;
  // final String? categoria;
  // final String? agrupacion;
  final String? vendedor;
  // final String? marcadd;
  // final String? lineaProduccion;
  // final String? ean;
  // final String? unidadesxcaja;
  // final String? archivo;
  // final int? saldo;
  // final String? grupo;
  // final int? iva;
  // final String? bodega;
  // final String? itf;
  // final String? unidades;
  // final String? subcategoria;
  // final String? core;
  // final String? portafolio;
  // final String? gm4;
  // final String? sublinea;
  // final String? pagaPastilla;
  // final String? clave;
  // final double? peso;
  // final String? cenExt2;
  // final String? sector;

  const ProductosCatalog({
    this.codigo,
    this.nombre,
    this.precio,
    // this.unidadMedida,
    // this.linea,
    // this.marca,
    // this.categoria,
    // this.agrupacion,
    this.vendedor,
    // this.marcadd,
    // this.lineaProduccion,
    // this.ean,
    // this.unidadesxcaja,
    // this.archivo,
    // this.saldo,
    // this.grupo,
    // this.iva,
    // this.bodega,
    // this.itf,
    // this.unidades,
    // this.subcategoria,
    // this.core,
    // this.portafolio,
    // this.gm4,
    // this.sublinea,
    // this.pagaPastilla,
    // this.clave,
    // this.peso,
    // this.cenExt2,
    // this.sector,
  });

  factory ProductosCatalog.fromJson(Map<String, dynamic> json) =>
      ProductosCatalog(
        codigo: json['Codigo'] ?? '',
        nombre: json['Nombre'] ?? '',
        precio: json['Precio'] ?? 0.0,
        // unidadMedida: json['Unidadmedida'] ?? '',
        // linea: json['Linea'] ?? '',
        // marca: json['Marca'] ?? '',
        // categoria: json['Categoria'] ?? '',
        // agrupacion: json['Agrupacion'] ?? '',
        vendedor: json['Vendedor'] ?? '',
        // marcadd: json['marcadd'] ?? '',
        // lineaProduccion: json['Lineaproduccion'] ?? '',
        // ean: json['Ean'] ?? '',
        // unidadesxcaja: json['Unidadesxcaja'] ?? '',
        // archivo: json['Archivo'] ?? '',
        // saldo: json['Saldo'] ?? 0,
        // grupo: json['Grupo'] ?? '',
        // iva: json['Iva'] ?? 0,
        // bodega: json['Bodega'] ?? '',
        // itf: json['Itf'] ?? '',
        // unidades: json['Unidades'] ?? '',
        // subcategoria: json['Subcategoria'] ?? '',
        // core: json['Core'] ?? '',
        // portafolio: json['Portafolio'] ?? '',
        // gm4: json['GM4'] ?? '',
        // sublinea: json['Sublinea'] ?? '',
        // pagaPastilla: json['Paga_pastilla'] ?? '',
        // clave: json['Clave'] ?? '',
        // peso: json['Peso'] ?? 0.0,
        // cenExt2: json['Cen_ext2'] ?? '',
        // sector: json['Sector'] ?? '',
      );
  @override
  List<Object?> get props => [
        codigo,
        nombre,
        precio,
        // unidadMedida,
        // linea,
        // marca,
        // categoria,
        // agrupacion,
        vendedor,
        // marcadd,
        // lineaProduccion,
        // ean,
        // unidadesxcaja,
        // archivo,
        // saldo,
        // grupo,
        // iva,
        // bodega,
        // itf,
        // unidades,
        // subcategoria,
        // core,
        // portafolio,
        // gm4,
        // sublinea,
        // pagaPastilla,
        // clave,
        // peso,
        // cenExt2,
        // sector,
      ];
}
