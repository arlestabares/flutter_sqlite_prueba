import 'package:equatable/equatable.dart';

class Client extends Equatable {
  // final String? codigo;
  final String? nombre;
  final String? razonSocial;
  // final String? nit;
  // final String? direccion;
  final String? ciudad;
  // final String? telefono;
  // final String? vendedor;
  // final String? fechaIngreso;
  // final String? rutaParada;
  // final String? canal;
  // final String? subcanal;
  // final double? cupo;
  // final String? plazo;
  // final String? ica;
  // final String? actividad;
  // final String? agencia;
  // final String? tipoDoc;
  // final String? linea;
  // final String? territorio;
  // final String? tipoCredito;
  // final String? codPadre;
  // final String? factor;
  // final String? bloqueado;
  // final String? sustituto;
  // final String? retencion;
  // final String? perIca;
  // final String? unidadMedida;
  // final String? ordencompra;
  // final String? cumpleanos;
  // final String? codigoAmarre;
  // final String? clienteCunit;
  // final String? clienteZenu;
  // final String? clienteSuizo;
  // final int? tipoCliente;
  // final String? vendedor1;
  // final String? ruta1;
  // final String? vendedor2;
  // final String? ruta2;
  // final String? vendedor3;
  // final String? ruta3;
  // final String? vendedor4;
  // final String? ruta4;
  // final String? vendedor5;
  // final String? ruta5;
  // final String? vendedor6;
  // final String? ruta6;
  // final double? latitud;
  // final double? longitud;
  // final String? grupoPrecios;
  // final String? condicionPago;
  // final String? factor2;
  // final String? digitoVer;
  // final String? barrio;
  // final String? nombres;
  // final String? apellidos;
  // final String? zonaVentas;
  // final String? zonaTransporte;
  // final String? enlace1;
  // final String? enlace2;
  // final String? enlace3;
  // final String? enlace4;
  // final String? enlace5;
  // final String? email;
  // final String? tipoRegimen;
  // final String? telefono2;
  // final String? celular;
  // final String? fax;
  // final String? dirEntrega;
  // final String? neocli;
  // final String? potencial;
  // final String? valor;
  // final String? necesidad;
  // final String? ciudad2;
  // final double? cupo1;
  // final double? cupo2;
  // final double? cupo3;
  // final double? cupo4;
  // final double? cupo5;
  // final double? cupoDisponible;
  // final String? tarjetaRegistro;
  // final String? actividadEconomica;
  // final String? tipimp;
  // final String? clasifcli;
  // final int? totalCore;
  // final String? atributo5;
  // final String? rutero;
  // final String? enlace;
  // final double? cupoc;

  const Client({
    // this.codigo,
    this.nombre,
    this.razonSocial,
    // this.nit,
    // this.direccion,
    this.ciudad,
    // this.telefono,
    // this.vendedor,
    // this.fechaIngreso,
    // this.rutaParada,
    // this.canal,
    // this.subcanal,
    // this.cupo,
    // this.plazo,
    // this.ica,
    // this.actividad,
    // this.agencia,
    // this.tipoDoc,
    // this.linea,
    // this.territorio,
    // this.tipoCredito,
    // this.codPadre,
    // this.factor,
    // this.bloqueado,
    // this.sustituto,
    // this.retencion,
    // this.perIca,
    // this.unidadMedida,
    // this.ordencompra,
    // this.cumpleanos,
    // this.codigoAmarre,
    // this.clienteCunit,
    // this.clienteZenu,
    // this.clienteSuizo,
    // this.tipoCliente,
    // this.vendedor1,
    // this.ruta1,
    // this.vendedor2,
    // this.ruta2,
    // this.vendedor3,
    // this.ruta3,
    // this.vendedor4,
    // this.ruta4,
    // this.vendedor5,
    // this.ruta5,
    // this.vendedor6,
    // this.ruta6,
    // this.latitud,
    // this.longitud,
    // this.grupoPrecios,
    // this.condicionPago,
    // this.factor2,
    // this.digitoVer,
    // this.barrio,
    // this.nombres,
    // this.apellidos,
    // this.zonaVentas,
    // this.zonaTransporte,
    // this.enlace1,
    // this.enlace2,
    // this.enlace3,
    // this.enlace4,
    // this.enlace5,
    // this.email,
    // this.tipoRegimen,
    // this.telefono2,
    // this.celular,
    // this.fax,
    // this.dirEntrega,
    // this.neocli,
    // this.potencial,
    // this.valor,
    // this.necesidad,
    // this.ciudad2,
    // this.cupo1,
    // this.cupo2,
    // this.cupo3,
    // this.cupo4,
    // this.cupo5,
    // this.cupoDisponible,
    // this.tarjetaRegistro,
    // this.actividadEconomica,
    // this.tipimp,
    // this.clasifcli,
    // this.totalCore,
    // this.atributo5,
    // this.rutero,
    // this.enlace,
    // this.cupoc,
  });

  factory Client.fromJson(Map<String, dynamic> json) => Client(
        // codigo: json['Codigo'],
        nombre: json['Nombre']?? '',
        razonSocial: json['Razonsocial']??'',
        // nit: json['Nit'] ??'',
        // direccion: json['Direccion'],
        ciudad: json['Ciudad']??'',
        // telefono: json['Telefono'],
        // vendedor: json['Vendedor'] ?? '',
        // fechaIngreso: json['FechaIngreso'] ?? '',
        // rutaParada: json['Ruta_parada'] ?? '',
        // canal: json['Canal'],
        // subcanal: json['Subcanal'],
        // cupo: json['Cupo'],
        // plazo: json['Plazo'] ?? '',
        // ica: json['ICA'] ?? '',
        // actividad: json['Actividad'],
        // agencia: json['Agencia'],
        // tipoDoc: json['TipoDoc'],
        // linea: json['Linea'],
        // territorio: json['Territorio'],
        // tipoCredito: json['TipoCredito'] ?? '',
        // codPadre: json['CodPadre'],
        // factor: json['Factor'],
        // bloqueado: json['Bloqueado'] ?? '',
        // sustituto: json['Sustituto'] ?? '',
        // retencion: json['Retencion'] ?? '',
        // perIca: json['PerIca'] ?? '',
        // unidadMedida: json['UnidadMedida'] ?? '',
        // ordencompra: json['OrdenCompra'] ?? "",
        // cumpleanos: json['Cumpleanos'] ?? '',
        // codigoAmarre: json['CodigoAmarre'],
        // clienteCunit: json['ClienteCunit'] ?? '',
        // clienteZenu: json['ClienteZenu'] ?? '',
        // clienteSuizo: json['ClienteSuizo'] ?? '',
        // tipoCliente: json['TipoCliente'] ?? 0,
        // vendedor1: json['Vendedor1'] ?? '',
        // ruta1: json['Ruta1'] ?? '',
        // vendedor2: json['Vendedor2'] ?? '',
        // ruta2: json['Ruta2'] ?? '',
        // vendedor3: json['Vendedor3'] ?? '',
        // ruta3: json['Ruta3'] ?? '',
        // vendedor4: json['Vendedor4'] ?? '',
        // ruta4: json['Ruta4'] ?? '',
        // vendedor5: json['Vendedor5'] ?? '',
        // ruta5: json['Ruta5'] ?? '',
        // vendedor6: json['Vendedor6'] ?? '',
        // ruta6: json['Ruta6'] ?? '',
        // latitud: json['latitud'] ?? 0.0,
        // longitud: json['longitud'] ?? 0.0,
        // grupoPrecios: json['GruposPrecios'] ?? '',
        // condicionPago: json['condicionpago'],
        // factor2: json['factor2'],
        // digitoVer: json['DigitoVer'] ?? '',
        // barrio: json['Barrio'] ?? '',
        // nombres: json['Nombres'] ?? '',
        // apellidos: json['Apellidos'] ?? '',
        // zonaVentas: json['Zonaventas'],
        // zonaTransporte: json['ZonaTransporte'],
        // enlace1: json['enlace1'] ?? '',
        // enlace2: json['enlace2'] ?? '',
        // enlace3: json['enlace3'] ?? '',
        // enlace4: json['enlace4'] ?? '',
        // enlace5: json['enlace5'] ?? '',
        // email: json['email'] ?? '',
        // tipoRegimen: json['tiporegimen'] ?? '',
        // telefono2: json['Telefono2'] ?? '',
        // celular: json['celular'] ?? '',
        // fax: json['fax'] ?? '',
        // dirEntrega: json['DirEntrega'] ?? '',
        // neocli: json['neocli'] ?? '',
        // potencial: json['Potencial'] ?? '',
        // valor: json['Valor'] ?? '',
        // necesidad: json['Necesidad'] ?? '',
        // ciudad2: json['ciudad2'] ?? '',
        // cupo1: json['Cupo1'] ?? 0.0,
        // cupo2: json['Cupo2'] ?? 0.0,
        // cupo3: json['Cupo3'] ?? 0.0,
        // cupo4: json['Cupo4'] ?? 0.0,
        // cupo5: json['Cupo5'] ?? 0.0,
        // cupoDisponible: json['Cupo_disponible'] ?? 0.0,
        // tarjetaRegistro: json['Tarjeta_registro'] ?? '',
        // actividadEconomica: json['Actividad_economica'] ?? '',
        // tipimp: json['tipimp'] ?? '',
        // clasifcli: json['clasifcli'] ?? '',
        // totalCore: json['totalcore'] ?? 0,
        // atributo5: json['atributo5'] ?? '',
        // rutero: json['rutero'] ?? '',
        // enlace: json['enlace'] ?? '',
        // cupoc: json['CUPOC'] ?? 0.0,
      );

  @override
  List<Object?> get props => [
        // codigo,
        nombre,
        razonSocial,
        // nit,
        // direccion,
        ciudad,
        // telefono,
        // vendedor,
        // fechaIngreso,
        // rutaParada,
        // canal,
        // subcanal,
        // cupo,
        // plazo,
        // ica,
        // actividad,
        // agencia,
        // tipoDoc,
        // linea,
        // territorio,
        // tipoCredito,
        // codPadre,
        // factor,
        // bloqueado,
        // sustituto,
        // retencion,
        // perIca,
        // unidadMedida,
        // ordencompra,
        // cumpleanos,
        // codigoAmarre,
        // clienteCunit,
        // clienteZenu,
        // clienteSuizo,
        // tipoCliente,
        // vendedor1,
        // ruta1,
        // vendedor2,
        // ruta2,
        // vendedor3,
        // ruta3,
        // vendedor4,
        // ruta4,
        // vendedor5,
        // ruta5,
        // vendedor6,
        // ruta6,
        // latitud,
        // longitud,
        // grupoPrecios,
        // condicionPago,
        // factor2,
        // digitoVer,
        // barrio,
        // nombres,
        // apellidos,
        // zonaVentas,
        // zonaTransporte,
        // enlace1,
        // enlace2,
        // enlace3,
        // enlace4,
        // enlace5,
        // email,
        // tipoRegimen,
        // telefono2,
        // celular,
        // fax,
        // dirEntrega,
        // neocli,
        // potencial,
        // valor,
        // necesidad,
        // ciudad2,
        // cupo1,
        // cupo2,
        // cupo3,
        // cupo4,
        // cupo5,
        // cupoDisponible,
        // tarjetaRegistro,
        // actividadEconomica,
        // tipimp,
        // clasifcli,
        // totalCore,
        // atributo5,
        // rutero,
        // enlace,
        // cupoc,
      ];
}
