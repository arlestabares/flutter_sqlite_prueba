import 'dart:developer';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/client.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/order.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/productos_catalogo.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/user.dart';
import 'package:flutter_sqlite/features/sqlite/data/models/vendedor.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:sqlbrite/sqlbrite.dart';
import 'package:synchronized/synchronized.dart';

class DatabaseHelper {
  static const _dataBaseName = 'DataBase.db';
  static const _databaseVersion = 1;

  static const orderTable = 'Order';
  static const orderId = 'orderId';
  static const usuarioTable = 'Usuario';
  static const clientsTable = 'Clientes';
  static const vendedorTable = 'Vendedor';
  static const productsCatalogTable = 'ProductosCatalogo';

  static late BriteDatabase _streamDatabase;
  static var lock = Lock();

  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

//solo existe una única referencia de la base de datos en toda la aplicación.
  static Database? _database;

  // void _onCreate(Database db) async {
  //   await db.delete('''
  //       DELETE TABLE $orderTable(
  //         $orderId INTEGER PRIMARY KEY,
  //         client TEXT,
  //         productName TEXT,
  //         productPrice REAL,
  //         productVendor TEXT,
  //         productOrderDate TEXT,
  //       )
  //       ''');
  // }
  // esto abre la base de datos (y la crea si no existe)
  Future<Database> _initDatabase() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = p.join(documentsDirectory.path, _dataBaseName);
    var data = await rootBundle.load('assets/db/DataBase.db');
    log('$data');
    List<int> bytes =
        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await File(path).writeAsBytes(bytes);
    Sqflite.setDebugModeOn(true);
    // _onCreate(Database);
    return openDatabase(path, version: _databaseVersion, onOpen: (instance) {});
  }

  Future<Database> get getDatabase async {
    if (_database != null) return _database!;
    // Usar este objeto para evitar el acceso simultáneo a los datos
    await lock.synchronized(
      () async {
        //crea una instancia perezosa de la base de datos la primera vez
        //que se accede a ella
        if (_database == null) {
          _database = await _initDatabase();
          _streamDatabase = BriteDatabase(_database!);
        }
      },
    );
    return _database!;
  }

  Future<Database> get getStreamDatabase async {
    await getDatabase;
    return _streamDatabase;
  }

  Future<int> insert(String table, Map<String, dynamic> row) async {
    final db = await instance.getDatabase;
    return db.insert(table, row);
  }

  Future<int> insertOrder(Order order) {
    return insert(orderTable, order.toJson());
  }

  List<Order> parseOrder(List<Map> orderList) {
    final orders = <Order>[];
    for (var orderMap in orderList) {
      final order = Order.fromJson(orderMap);
      orders.add(order);
    }
    return orders;
  }

  Future<List<Order>> getAllOrder() async {
    final db = await instance.getDatabase;
    final orderMapList = await db.query(orderTable);
    final orderList = parserOrder(orderMapList);
    return orderList;
  }

  Future<List<User>> getAllUsers() async {
    final db = await instance.getDatabase;
    final productsMapList = await db.query(usuarioTable);
    final usuariosList = parseUsers(productsMapList);
    return usuariosList;
  }

  Future<List<ProductosCatalog>> getAllProductsCatalog() async {
    final db = await instance.getDatabase;
    final productsCatalogMapList = await db.query(productsCatalogTable);
    final productsList = parseProductsCatalog(productsCatalogMapList);
    return productsList;
  }

  Future<List<Client>> getAllClients() async {
    final db = await instance.getDatabase;
    final clientMapList = await db.query(clientsTable);
    final clientList = parseClients(clientMapList);
    return clientList;
  }

  Future<List<Vendedor>> getAllVendors() async {
    final db = await instance.getDatabase;
    final vendedorMapList = await db.query(vendedorTable);
    final vendedorList = parseVendors(vendedorMapList);
    return vendedorList;
  }

  Future<int> _delete(String table, String columnId, int id) async {
    final db = await instance.getStreamDatabase;
    return db.delete(orderTable, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> deleteOrder(Order order) async {
    if (order.id != null) {
      return _delete(orderTable, orderId, order.id!);
    } else {
      return Future.value(-1);
    }
  }

  List<Order> parserOrder(List<Map<String, dynamic>> orderMapList) {
    final ordersList = <Order>[];
    for (var orderMap in orderMapList) {
      final order = Order.fromJson(orderMap);
      ordersList.add(order);
    }
    return ordersList;
  }

  List<User> parseUsers(List<Map<String, dynamic>> usuariosMapList) {
    final usuariosList = <User>[];
    for (var usuarioMap in usuariosMapList) {
      final usuario = User.fromJson(usuarioMap);
      usuariosList.add(usuario);
    }
    return usuariosList;
  }

  List<ProductosCatalog> parseProductsCatalog(
      List<Map<String, dynamic>> productsCatalogMapList) {
    final productsList = <ProductosCatalog>[];
    for (var productCatalog in productsCatalogMapList) {
      final product = ProductosCatalog.fromJson(productCatalog);
      productsList.add(product);
    }
    return productsList;
  }

  List<Client> parseClients(List<Map<String, dynamic>> clientsMapList) {
    final clientsList = <Client>[];
    for (var clientL in clientsMapList) {
      final client = Client.fromJson(clientL);
      clientsList.add(client);
    }
    return clientsList;
  }

  List<Vendedor> parseVendors(List<Map<String, dynamic>> vendedorMapList) {
    final vendedoresList = <Vendedor>[];
    for (var vendedorL in vendedorMapList) {
      final vendedor = Vendedor.fromJson(vendedorL);
      vendedoresList.add(vendedor);
    }
    return vendedoresList;
  }

  List<User> parseUser(List<Map<String, dynamic>> userMapList) {
    final usersList = <User>[];
    for (var userL in userMapList) {
      final user = User.fromJson(userL);
      usersList.add(user);
    }
    return usersList;
  }

  void close() {
    _database?.close();
  }
}
