import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/memory_repository.dart';
import 'package:flutter_sqlite/features/sqlite/data/repositories/repository.dart';

class MultiRepositoryProviderWidget extends StatelessWidget {
  const MultiRepositoryProviderWidget({
    Key? key,
    required this.child,
    required this.repository,
    required this.mRepository,
  }) : super(key: key);
  final Widget child;
  final Repository repository;
  final MemoryRepository mRepository;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<Repository>(
          lazy: false,
          create: (context) => repository,
        ),
        RepositoryProvider<MemoryRepository>(
          lazy: false,
          create: (context) => mRepository,
        ),
        RepositoryProvider<MemoryRepository>(
          lazy: false,
          create: (context) => MemoryRepository(),
        ),
      ],
      child: child,
    );
  }
}
