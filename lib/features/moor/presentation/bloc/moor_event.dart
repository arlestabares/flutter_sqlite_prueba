part of 'moor_bloc.dart';

abstract class MoorEvent extends Equatable {
  const MoorEvent();

  @override
  List<Object> get props => [];
}
