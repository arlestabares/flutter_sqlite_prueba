import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'moor_event.dart';
part 'moor_state.dart';

class MoorBloc extends Bloc<MoorEvent, MoorState> {
  MoorBloc() : super(MoorInitial()) {
    on<MoorEvent>((event, emit) {});
  }
}
