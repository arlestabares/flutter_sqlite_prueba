part of 'moor_bloc.dart';

abstract class MoorState extends Equatable {
  const MoorState();  

  @override
  List<Object> get props => [];
}
class MoorInitial extends MoorState {}
