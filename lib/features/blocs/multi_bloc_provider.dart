import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/bloc/sqlite_bloc.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SqliteBloc>(create: (context) => SqliteBloc()),
      ],
      child: child,
    );
  }
}
