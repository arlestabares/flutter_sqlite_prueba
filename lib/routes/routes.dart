import 'package:flutter/material.dart';
import 'package:flutter_sqlite/features/moor/presentation/pages/home_moor_page.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/pages/home_sqlite_page.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/details_view.dart';
import 'package:flutter_sqlite/features/sqlite/presentation/views/product_catalog_view.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home_sqlite_page': (_) => const HomeSqlitePage(),
  'home_moor_page': (_) => const HomeMoorPage(),
  'details_view': (_) => const DetailsView(),
  'product_catalog': (_) => const ProductCatalogView(),
};
